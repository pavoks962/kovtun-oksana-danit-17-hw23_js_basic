
"use strict"

const form = document.createElement('form');
form.innerHTML = `
<form action="">
<input type="number" value="" class="input" placeholder="Price">
</form>
`
document.body.append(form)
const input = document.querySelector('.input')


const div =  document.createElement('div');
div.className = 'div';
const span = document.createElement('span');
span.className = 'span';
div.append(span);
const btn = document.createElement('button');
btn.innerText = 'X';
span.after(btn);
const error = document.createElement('span');
error.innerText = 'Please enter correct price!';


input.addEventListener('blur', event => {
    input.classList.remove('focusin');
    if (input.value <= 0) {
        input.style.borderColor = 'red';
        input.after(error);
        return
    }
    input.before(div);
    span.innerText = `Поточна ціна: ${input.value} грн.`;   
 })

input.onfocus = function () {
    input.classList.add('focusin');
}
    


